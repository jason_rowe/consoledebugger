﻿using System;
using System.Diagnostics;
using System.IO;
using log4net;
using log4net.Config;

namespace ConsoleDebugger
{
	internal class Program
	{
		private static readonly ILog Log = LogManager.GetLogger(typeof(Program));

		private static void Main(string[] args)
		{
			XmlConfigurator.Configure();

			var cmd = new LaunchCommandAsProcess(string.Join(" ", args));
			cmd.OutputReceived += launch_OutputReceived;
			cmd.SyncClose();
		}

		/// Outputs normal and error output from the command prompt.
		private static void launch_OutputReceived(object sendingProcess, EventArgsForCommand e)
		{
			Log.Info(e.OutputData);
			Console.WriteLine(e.OutputData);
		}
	}

	public class LaunchCommandAsProcess
	{
		private static readonly ILog Log = LogManager.GetLogger(typeof(Program));

		public delegate void OutputEventHandler(object sendingProcess, EventArgsForCommand e);
		public event OutputEventHandler OutputReceived;
		private readonly StreamWriter stdIn;
		private readonly Process p;

		public void SendCommand(string command)
		{
			Log.Info(command);
			stdIn.WriteLine(command);
		}

		public LaunchCommandAsProcess(string args)
		{
			p = new Process
				{
					StartInfo =
						{
							FileName = @"C:\Program Files (x86)\Git\cmd\git.exe",
							Arguments = args,
							UseShellExecute = false,
							RedirectStandardInput = true,
							RedirectStandardOutput = true,
							RedirectStandardError = true,
							CreateNoWindow = true,
						}
				};
			p.Start();

			stdIn = p.StandardInput;
			p.OutputDataReceived += Process_OutputDataReceived;
			p.ErrorDataReceived += Process_OutputDataReceived;
			p.BeginOutputReadLine();
			p.BeginErrorReadLine();
		}

		///
		/// Raises events when output data has been received. Includes normal and error output.
		/// 
		private void Process_OutputDataReceived(object sendingProcess, DataReceivedEventArgs outLine)
		{
			if (outLine.Data == null)
				return;

			if (outLine.Data != null)
			{
				var e = new EventArgsForCommand {OutputData = outLine.Data};
				OutputReceived(this, e);
			}
		}

		///
		/// Synchronously closes the command promp.
		/// 
		public void SyncClose()
		{
			stdIn.WriteLine("exit");
			p.WaitForExit();
			p.Close();
		}

		///
		/// Asynchronously closees the command prompt.
		/// 
		public void AsyncClose()
		{
			stdIn.WriteLine("exit");
			p.Close();
		}
	}

	public class EventArgsForCommand : EventArgs
	{
		public string OutputData { get; internal set; }
	}
}
